package info.hccis.TutorApp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity {
    //timer in miliseconds, 1000ms = 1s//
    private static int SPLASH_TIME_OUT = 4000;
    ImageView imageViewLogo;
    ImageView imageViewBg;
    TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_intro);

        new Handler().postDelayed(new Runnable() {
            //showing splashscreen with a timer //

            @Override
            public void run() {
                //this is executed once the timer is over//

                Intent i = new Intent(SplashScreenActivity.this,    MainActivity.class);
                startActivity(i);
                finish();

            }
        },SPLASH_TIME_OUT);
        imageViewLogo = findViewById(R.id.splashLogo);
        textViewTitle = findViewById(R.id.textViewTitle);
        imageViewBg = findViewById(R.id.splashBg);

        imageViewBg.animate().translationY(-1600).setDuration(1000).setStartDelay(4000);
        imageViewLogo.animate().translationY(-1400).setDuration(1000).setStartDelay(4000);
        textViewTitle.animate().translationY(-1400).setDuration(1000).setStartDelay(4000);
}
}
